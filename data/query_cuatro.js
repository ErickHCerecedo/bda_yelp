/**
  * Query 4: Determine el número de restaurantes en un radio de 2 km con referencia a las coordenadas que obtuvoanteriormente.
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Location = Toronto
    *   Categorias = Restaurants
    * count() = Muestra los resultados en formato estetico.
    */
    db.businesses.find({location:{ $geoWithin: { $centerSphere : [[-79.4264954, 43.6427923], 2/6378.1]}}}).count()

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
  {
    2061
  }
