/**
  * Query 6: Muestre los restaurantes“top 5”(en base al número de estrellas) que sirven hamburguesas, al que puedo ir un jueves a las 16:00, en un radio de 3 km, con referencia a la Universidad de Toronto, CampusSt. George.
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Location    = n un radio de 3 km, con referencia a la Universidad de Toronto CampusSt. George
    *   Categorias  = Burgers
    *   is_open     = open
    * sort()    = Ordena de mayor a menor
    * limit(5)  = Los mejores 5
    * pretty()  = Resultado estetico
    */
    db.businesses.find({
      location:{ $geoWithin: { $centerSphere: [[-79.4264954, 43.6427923], 3/6378.1] }},
      categories: {$regex:".Burgers."},
      is_open: 1,
    }).sort({
      stars: -1
    }).limit(5).pretty()


  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
    {
  	"_id" : ObjectId("5cd227b7766bae1e0495b214"),
  	"business_id" : "PUNSBV03IQDNSOmsCsGPOQ",
  	"name" : "The Mutt",
  	"address" : "1A-1516 Dundas Street W",
  	"city" : "Toronto",
  	"state" : "ON",
  	"postal_code" : "M6K 1T5",
  	"latitude" : 43.6498194,
  	"longitude" : -79.4326104,
  	"stars" : 5,
  	"review_count" : 5,
  	"is_open" : 1,
  	"attributes" : {
  		"RestaurantsPriceRange2" : "2",
  		"GoodForDancing" : "False"
  	},
  	"categories" : "Pubs, Nightlife, Burgers, Beer Bar, Bars, Restaurants",
  	"hours" : {
  		"Monday" : "17:0-0:0",
  		"Wednesday" : "17:0-0:0",
  		"Thursday" : "17:0-2:0",
  		"Friday" : "17:0-2:0",
  		"Saturday" : "17:0-2:0",
  		"Sunday" : "17:0-0:0"
  	},
  	"location" : {
  		"type" : "Point",
  		"coordinates" : [
  			-79.4326104,
  			43.6498194
  		]
  	}
  }
  {
  	"_id" : ObjectId("5cd227b4766bae1e04947fb7"),
  	"business_id" : "qNCt4Dx3S0R-n3x39mxxLw",
  	"name" : "Tilt Arcade Bar",
  	"address" : "824 Dundas Street West",
  	"city" : "Toronto",
  	"state" : "ON",
  	"postal_code" : "M6J 1V3",
  	"latitude" : 43.6517367,
  	"longitude" : -79.409256,
  	"stars" : 4.5,
  	"review_count" : 35,
  	"is_open" : 1,
  	"attributes" : {
  		"GoodForKids" : "True",
  		"RestaurantsGoodForGroups" : "True",
  		"RestaurantsReservations" : "False",
  		"RestaurantsPriceRange2" : "1",
  		"Music" : "{'dj': False, 'background_music': False, 'no_music': False, 'jukebox': False, 'live': True, 'video': False, 'karaoke': False}",
  		"GoodForDancing" : "False",
  		"HasTV" : "False",
  		"WiFi" : "'no'",
  		"Ambience" : "{'touristy': False, 'hipster': False, 'romantic': False, 'intimate': False, 'trendy': False, 'upscale': False, 'classy': False, 'casual': False}",
  		"Alcohol" : "'full_bar'"
  	},
  	"categories" : "Nightlife, Arts & Entertainment, Restaurants, Bars, Burgers, Arcades",
  	"hours" : {
  		"Monday" : "18:0-2:0",
  		"Tuesday" : "18:0-2:0",
  		"Wednesday" : "18:0-2:0",
  		"Thursday" : "18:0-2:0",
  		"Friday" : "18:0-2:0",
  		"Saturday" : "16:0-2:0",
  		"Sunday" : "16:0-2:0"
  	},
  	"location" : {
  		"type" : "Point",
  		"coordinates" : [
  			-79.409256,
  			43.6517367
  		]
  	}
  }
  {
  	"_id" : ObjectId("5cd227b5766bae1e0494ae0c"),
  	"business_id" : "jrkzgoLCvk_IRw5e-3Zyrw",
  	"name" : "32 Chicken St.",
  	"address" : "409 College Street",
  	"city" : "Toronto",
  	"state" : "ON",
  	"postal_code" : "M5T 1T1",
  	"latitude" : 43.6563673,
  	"longitude" : -79.4065093,
  	"stars" : 4.5,
  	"review_count" : 9,
  	"is_open" : 1,
  	"attributes" : null,
  	"categories" : "Chicken Shop, Burgers, Chicken Wings, Fast Food, Korean, Asian Fusion, Restaurants",
  	"hours" : null,
  	"location" : {
  		"type" : "Point",
  		"coordinates" : [
  			-79.4065093,
  			43.6563673
  		]
  	}
  }
  {
  	"_id" : ObjectId("5cd227b7766bae1e0495971c"),
  	"business_id" : "ft5ggObCvKvAiShsb1g_lA",
  	"name" : "Extra Burger",
  	"address" : "1357 Dundas Street W",
  	"city" : "Toronto",
  	"state" : "ON",
  	"postal_code" : "M6J 1Y3",
  	"latitude" : 43.6493683,
  	"longitude" : -79.4275516,
  	"stars" : 4.5,
  	"review_count" : 8,
  	"is_open" : 1,
  	"attributes" : {
  		"BusinessParking" : "{'garage': False, 'street': False, 'validated': False, 'lot': False, 'valet': False}"
  	},
  	"categories" : "Fast Food, Burgers, Restaurants",
  	"hours" : null,
  	"location" : {
  		"type" : "Point",
  		"coordinates" : [
  			-79.4275516,
  			43.6493683
  		]
  	}
  }
  {
  	"_id" : ObjectId("5cd227b8766bae1e04967d76"),
  	"business_id" : "RdLgUQf0vOsfYA8t9MRB3A",
  	"name" : "3 Brasseurs",
  	"address" : "2 Liberty Street",
  	"city" : "Toronto",
  	"state" : "ON",
  	"postal_code" : "M6K 3E7",
  	"latitude" : 43.6383875842,
  	"longitude" : -79.4200811908,
  	"stars" : 4.5,
  	"review_count" : 3,
  	"is_open" : 1,
  	"attributes" : {
  		"OutdoorSeating" : "True",
  		"GoodForKids" : "False",
  		"RestaurantsAttire" : "u'casual'",
  		"DriveThru" : "False",
  		"RestaurantsReservations" : "True",
  		"HasTV" : "True",
  		"Alcohol" : "u'full_bar'",
  		"RestaurantsTakeOut" : "True",
  		"RestaurantsPriceRange2" : "2",
  		"BusinessParking" : "{'garage': False, 'street': False, 'validated': False, 'lot': False, 'valet': False}",
  		"NoiseLevel" : "u'average'",
  		"Ambience" : "{'touristy': False, 'hipster': False, 'romantic': False, 'intimate': False, 'trendy': False, 'upscale': False, 'classy': False, 'casual': False}",
  		"RestaurantsGoodForGroups" : "True",
  		"WiFi" : "'free'",
  		"RestaurantsTableService" : "True",
  		"RestaurantsDelivery" : "False",
  		"GoodForMeal" : "{'dessert': False, 'latenight': False, 'lunch': False, 'dinner': True, 'brunch': False, 'breakfast': False}"
  	},
  	"categories" : "Salad, Sandwiches, Restaurants, Bars, Burgers, Nightlife",
  	"hours" : {
  		"Monday" : "11:0-0:0",
  		"Tuesday" : "11:0-0:0",
  		"Wednesday" : "11:0-0:0",
  		"Thursday" : "11:0-1:0",
  		"Friday" : "11:0-1:0",
  		"Saturday" : "11:0-1:0",
  		"Sunday" : "11:0-0:0"
  	},
  	"location" : {
  		"type" : "Point",
  		"coordinates" : [
  			-79.4200811908,
  			43.6383875842
  		]
  	}
  }
