/**
  * Query 1: Determinar el número de negocios registrados en el dataset en la ciudad de Toronto.
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Ciudad = Toronto
    *
    * count() = Realiza un conteo de todos los resultados.
    */
  db.businesses.find({city:"Toronto"}).count()

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
  {
    18906
  }
