/**
  * Query 2: Determinar el número de estos negocios correspondientes a restaurantes.
  */

  // Se crea un index para mejorar la eficiencia de la busqueda
  db.businesses.createIndex({city:"text", categories:"text"})

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Ciudad = Toronto
    *   Categorias = Restaurants
    * count() = Realiza un conteo de todos los resultados.
    */
  db.businesses.find({city:"Toronto",$text:{$search:"Restaurants"}}).count()

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
  {
    7965
  }
