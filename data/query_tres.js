/**
  * Query 3: Determine el número de restaurantes que están registrados como cerrados en la ciudad de Toronto.
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Ciudad = Toronto
    *   Categorias = Restaurants
    *   Is_open = Cerrado
    * count() = Realiza un conteo de todos los resultados.
    */
  db.businesses.find({city:"Toronto",$text:{$search:"Restaurants"},is_open:0}).count()

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
  {
    2711
  }
