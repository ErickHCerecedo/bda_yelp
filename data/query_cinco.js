/**
  * Query 5: Determine el número de restaurantes con [1, 2), [2,3), [3, 4), [4,5] estrellasen todo Toronto
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   Match: Son los parametrsde busqueda.
    *       Categories  = Restaurants
    *       city        = Toronto
    *   Bucket: Agrupacion y Salida de datos
    *       groupBy     = Stars
    *       boundaries  = Para tomar enteros
    *       Ouput       = Mostrar el numero de resultados en "Count"
    * aggregate() = Para poder agrupar los parametros
    */
    db.businesses.aggregate([
        {
            $match : {
                categories : { $regex : ".Restaurants." },
                city: "Toronto",
            }
        },
        {
            $bucket : {
                groupBy:"$stars",
                boundaries: [ 1, 2, 3, 4, 5 ],
                default: 5,
                output: {
                  "count": { $sum: 1 },
                }
            }
        }
    ])

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
    { "_id" : 1, "count" : 55 }
    { "_id" : 2, "count" : 407 }
    { "_id" : 3, "count" : 1378 }
    { "_id" : 4, "count" : 1113 }
    { "_id" : 5, "count" : 78 }
