/**
  * Query 7: Liste 20 lugares (si existen) donde puedo comer “burritos”en la ciudad de Toronto
  */

  /**
    * Consulta: Coleccion businesses con parametros.
    *   city = Toronto
    *   categories = burritos
    * sort()  = Ordena los resultados
    * limit() = 20 resultados
    */
    db.businesses.find({
      city:"Toronto",
      categories: {$regex:"burritos"},
    }).sort({
      stars: -1
    }).limit(20)

  /**
    * Resultado:
    *   count() regresa un resultado de tipo numerico.
    */
